import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the SendProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SendProvider {
  url = "http://35.185.188.36";
  constructor(public http: Http) {
    console.log('Hello SendProvider Provider');
  }

  login(username,password) {
    return Observable.create(observer => {
        let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        let options = new RequestOptions({ headers: headers });
        let dt = {
          "username" : username,
          "password" : password
        }

        this.http.post(this.url + `/maps/login.php`, dt , options)
          .subscribe(data => {
            console.log(data);
            observer.next(data);
            observer.complete();
           }, error => {
             console.log(error);
            observer.next(error.ok);
            observer.complete();
          });
      });
  }

  getAddress(latlng) {
    let url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+ latlng +'&sensor=true/false';
    return Observable.create(observer => {
        this.http.get(url)
        .subscribe(data => {
            let result = JSON.parse(data['_body']);
            console.log(result);
            observer.next({allow : data.ok, result : result });
            observer.complete();
         }, error => {
           observer.next(error.ok);
           observer.complete();
        });
    });
  }

  save(position,name, jalan){
    var ls = JSON.parse(localStorage.getItem('list'));
    if(ls == null) {
      ls = [];
    }

    var data = {
      nama : name,
      jalan : jalan,
      lat : position.latitude,
      lng : position.longitude,
      id : ls[ls.length-1] == undefined ? 1 : ls[ls.length-1].id+1,
      login : localStorage.getItem('login'),
      time : this.formatDate(),
    }
    ls.push(data);

    localStorage.setItem('list',JSON.stringify(ls));
  }

  formatDate(){
    var d = new Date();
    var date = d.getDate();
    var month = formatMonth(d.getMonth());
    var year = d.getFullYear();
    var hour = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();

    return year+ "/"+month+"/"+date+" "+hour+":"+minutes+":"+seconds;

    function formatMonth(month){
      month = month+1;
      if(month < 10){
        return "0"+month;
      }
      else{
        return month+1;
      }
    }
  }

  getLocations() {
    return JSON.parse(localStorage.getItem('list'));
  }

  post() {
      return Observable.create(observer => {
        let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        let options = new RequestOptions({ headers: headers });
        let dt = JSON.parse(localStorage.getItem('list'));

        this.http.post(this.url + `/maps/input_lokasi_mobile.php`, dt , options)
          .subscribe(data => {
            console.log(data);
            observer.next(data.ok);
            observer.complete();
           }, error => {
             console.log(error);
            observer.next(error.ok);
            observer.complete();
          });
      });
  }

}
