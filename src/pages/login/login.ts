import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SendProvider } from '../../providers/send/send';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    loading;
    username;
    password;
  constructor(public navCtrl: NavController, public alertCtrl:AlertController,public navParams: NavParams, public sendProvider:SendProvider,private loadingCtrl: LoadingController) {
      this.showLoading();
      if(localStorage.getItem('login') != undefined) {
          this.navCtrl.setRoot(TabsPage,{});
      }
      this.loading.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  submitForm(){
      this.showLoading();
      this.sendProvider.login(this.username,this.password).subscribe(data => {
          if(data.ok) {
            data = JSON.parse(data['_body']);
            if(data.result) {
                localStorage.setItem('login',this.username);
                this.navCtrl.setRoot(TabsPage,{});
            }
            else{
                this.showError('Login Failed');
            }
          }
          else{
            this.showError('Login Failed');
          }
      });
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
