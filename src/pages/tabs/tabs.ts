import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { NavController } from 'ionic-angular';
import { ListItemPage } from '../list-item/list-item';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ListItemPage;
  // tab3Root = ContactPage;

  constructor(public nav : NavController) {

  }

  logout() {
    localStorage.removeItem('login');
    this.nav.setRoot(LoginPage);
  }
}
