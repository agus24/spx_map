import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { SendProvider } from '../../providers/send/send';

declare var google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  pos = {
    "longitude" : 0,
    "latitude" : 0
  };
  alamat;
  jalan;
  loading;

  constructor(public navCtrl: NavController,
               public geolocation:Geolocation,
               public sendProvider:SendProvider,
               private alertCtrl: AlertController,
               public loadingCtrl : LoadingController) {
    this.loading = this.loadingCtrl;
  }

  ionViewDidLoad(){
    this.loadMap();
  }

  send() {
    this.sendProvider.save(this.pos, this.alamat, this.jalan);
  }

  sync() {
     this.showLoading();
     if(localStorage.getItem('list') == null){
       this.showError("List Anda Kosong");
     }
     this.sendProvider.post().subscribe(allowed => {
       if(allowed) {
         this.alertCtrl.create({
            title: 'Success',
            subTitle: "Data Berhasil Di sinkronisasi",
            buttons: ['OK']
          });
         localStorage.removeItem('list');
         this.loading.dismiss();
       }
       else {
         this.showError("Terjadi Kesalahan Saat Sinkronisasi / tidak terhubung ke wifi superex");
       }
     });
  }

  loadMap(){
    let option = {
        enableHighAccuracy: true
    };
    this.geolocation.getCurrentPosition(option).then((position) => {
      this.pos.longitude = position.coords.longitude;
      this.pos.latitude = position.coords.latitude;
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let ltlng = this.pos.latitude + ","+this.pos.longitude;
      this.sendProvider.getAddress(ltlng).subscribe((result) => {
        if(result.allow){
            this.jalan = result.result.results[0].formatted_address;
            console.log(result);
        }
        else{
            this.showError("Cannot Get Data");
        }
      });
      console.log(this.jalan);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }, (err) => {
      let alert = this.alertCtrl.create({
        title: 'Fail',
        subTitle: "Terjadi Kesalahan Saat Loading Map",
        buttons: ['OK']
      });
      alert.present(prompt);
    });
    this.addMarker();
  }

  addMarker(){
    let content = "<h4>Information!</h4>";
    console.log('masuk');
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      title : content
    });
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
