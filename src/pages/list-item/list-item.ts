import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ListItemPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-item',
  templateUrl: 'list-item.html',
})
export class ListItemPage {
    locations :any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    this.locations = JSON.parse(localStorage.getItem('list'));
  }

  remove(id) {
      var index = this.filter(id);
      console.log(index);
      this.locations.splice(index,1);
      localStorage.setItem('list',JSON.stringify(this.locations));
  }

  filter(id) {
      for(var i=0; i < this.locations.length ; i++) {
          if(this.locations[i].id == id) {
              return i;
          }
      }
      return -1;
  }

}
